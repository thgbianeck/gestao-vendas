package br.com.bianeck.gvendas.gestaovendas.exceptions;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@ControllerAdvice
public class GestaoVendasExceptionHandler extends ResponseEntityExceptionHandler {

    public static final String CONSTANT_VALIDATION_NOT_BLANK = "NotBlank";
    public static final String CONSTANT_VALIDATION_LENGTH = "Length";

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {

        List<Errors> errors = generateErrorList(ex.getBindingResult());
        return handleExceptionInternal(ex, errors, headers, HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(EmptyResultDataAccessException.class)
    public ResponseEntity<Object> handleEmptyResultDataAccessException(
            EmptyResultDataAccessException ex, WebRequest request) {
        final String msgUsuario = "Recurso não encontrado.";
        final String msgDesenvolvedor = ex.toString();

        List<Errors> errors = List.of(new Errors(msgUsuario, msgDesenvolvedor));
        return handleExceptionInternal(ex, errors, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(RegraNegocioException.class)
    public ResponseEntity<Object> handleRegraNegocioException(RegraNegocioException ex, WebRequest request) {
        final String msgUsuario = ex.getMessage();
        final String msgDesenvolvedor = ex.getMessage();

        List<Errors> errors = List.of(new Errors(msgUsuario, msgDesenvolvedor));
        return handleExceptionInternal(ex, errors, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    private List<Errors> generateErrorList(BindingResult bindingResult) {

        return bindingResult.getFieldErrors().stream()
                .map(fieldError -> new Errors(userHandleErrorMessage(fieldError), fieldError.toString()))
                .collect(Collectors.toList());
    }

    private String userHandleErrorMessage(FieldError fieldError) {
        if(Objects.equals(fieldError.getCode(), CONSTANT_VALIDATION_NOT_BLANK)){
            return fieldError.getDefaultMessage().concat(" é obrigatório.");
        }
        if(Objects.equals(fieldError.getCode(), CONSTANT_VALIDATION_LENGTH)){
            return fieldError.getDefaultMessage()
                    .concat(String.format(" deve ter entre %s e %s caracteres.",
                            Objects.requireNonNull(fieldError.getArguments())[2],
                            fieldError.getArguments()[1]));
        }
        return fieldError.toString();
    }
}

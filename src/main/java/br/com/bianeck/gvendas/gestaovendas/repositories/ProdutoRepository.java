package br.com.bianeck.gvendas.gestaovendas.repositories;

import br.com.bianeck.gvendas.gestaovendas.entities.Categoria;
import br.com.bianeck.gvendas.gestaovendas.entities.Produto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ProdutoRepository extends JpaRepository<Produto, Long> {

    List<Produto> findByCategoriaCodigo(Long codigoCategoria);

    @Query("SELECT prod FROM Produto prod " +
            "WHERE prod.codigo =:codigo " +
            "AND prod.categoria.codigo =:codigoCategoria")
    Optional<Produto> findByCode(Long codigo, Long codigoCategoria);
}

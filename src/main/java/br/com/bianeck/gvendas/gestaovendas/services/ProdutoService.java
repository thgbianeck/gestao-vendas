package br.com.bianeck.gvendas.gestaovendas.services;

import br.com.bianeck.gvendas.gestaovendas.entities.Categoria;
import br.com.bianeck.gvendas.gestaovendas.entities.Produto;
import br.com.bianeck.gvendas.gestaovendas.repositories.ProdutoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class ProdutoService {

    private final ProdutoRepository produtoRepository;

    public List<Produto> listAll(Long codigoCategoria) {
        return produtoRepository.findByCategoriaCodigo(codigoCategoria);
    }

    public Optional<Produto> findByCode(Long codigo, Long codigoCategoria) {
        return produtoRepository.findByCode(codigo, codigoCategoria);
    }

    public Produto save(Produto produto) {
        return produtoRepository.save(produto);
    }
}

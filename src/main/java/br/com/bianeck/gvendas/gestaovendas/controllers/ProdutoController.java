package br.com.bianeck.gvendas.gestaovendas.controllers;

import br.com.bianeck.gvendas.gestaovendas.entities.Produto;
import br.com.bianeck.gvendas.gestaovendas.services.ProdutoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Api(tags = "Produto")
@RestController
@RequestMapping("/categoria/{codigoCategoria}/produto")
public class ProdutoController {

    @Autowired
    private ProdutoService produtoService;

    @ApiOperation(value = "Listar", nickname = "listAllProduto")
    @GetMapping
    public List<Produto> listAllProduto(@PathVariable Long codigoCategoria) {
        return produtoService.listAll(codigoCategoria);
    }
    @ApiOperation(value = "Listar por código", nickname = "findByCodeProduto" )
    @GetMapping("/{codigo}")
    public ResponseEntity<Optional<Produto>> findByCodeProduto(@PathVariable Long codigoCategoria, @PathVariable Long codigo) {
        Optional<Produto> produto = produtoService.findByCode(codigo, codigoCategoria);
        return produto.isPresent() ? ResponseEntity.ok(produto) : ResponseEntity.notFound().build();
    }

    @ApiOperation(value = "Salvar", nickname = "saveProduto" )
    @PostMapping
    public ResponseEntity<Produto> saveProduto(@RequestBody Produto produto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(produtoService.save(produto));
    }
}

package br.com.bianeck.gvendas.gestaovendas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EntityScan(basePackages = {"br.com.bianeck.gvendas.gestaovendas.entities"})
@EnableJpaRepositories(basePackages = {"br.com.bianeck.gvendas.gestaovendas.repositories"})
@ComponentScan(basePackages = {
		"br.com.bianeck.gvendas.gestaovendas.services",
		"br.com.bianeck.gvendas.gestaovendas.controllers",
		"br.com.bianeck.gvendas.gestaovendas.exceptions"})
@SpringBootApplication
public class GestaoVendasApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestaoVendasApplication.class, args);
	}

}

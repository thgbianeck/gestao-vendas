package br.com.bianeck.gvendas.gestaovendas.controllers;

import br.com.bianeck.gvendas.gestaovendas.entities.Categoria;
import br.com.bianeck.gvendas.gestaovendas.services.CategoriaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Api(tags = "Categoria")
@RequiredArgsConstructor
@RestController
@RequestMapping("/categoria")
public class CategoriaController {

    private final CategoriaService categoriaService;

    @ApiOperation(value = "Listar")
    @GetMapping
    public List<Categoria> listAll() {
        return categoriaService.listAll();
    }

    @ApiOperation(value = "Listar por código", nickname = "findByIdCategoria")
    @GetMapping("{codigo}")
    public ResponseEntity<Optional<Categoria>> findByIdCategoria(@PathVariable Long codigo) {
        Optional<Categoria> categoria = categoriaService.findByCode(codigo);

        return categoria.isPresent()
                ? ResponseEntity.ok(categoria)
                : ResponseEntity.notFound().build();
    }

    @ApiOperation(value = "Salvar", nickname = "saveCategoria")
    @PostMapping
    public ResponseEntity<Categoria> saveCategoria(@Valid @RequestBody Categoria categoria) {
        Categoria categoriaSalva = categoriaService.save(categoria);
        return ResponseEntity.status(HttpStatus.CREATED).body(categoriaSalva);
    }

    @ApiOperation(value = "Atualizar", nickname = "updateCategoria")
    @PutMapping("{codigo}")
    public ResponseEntity<Categoria> updateCategoria(@PathVariable Long codigo, @Valid @RequestBody Categoria categoria) {
        return ResponseEntity.ok(categoriaService.update(codigo, categoria));
    }

    @ApiOperation(value = "Deletar", nickname = "deleteCategoria")
    @DeleteMapping("{codigo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCategoria(@PathVariable Long codigo) {
        categoriaService.delete(codigo);
    }
}

package br.com.bianeck.gvendas.gestaovendas.services;

import br.com.bianeck.gvendas.gestaovendas.entities.Categoria;
import br.com.bianeck.gvendas.gestaovendas.exceptions.RegraNegocioException;
import br.com.bianeck.gvendas.gestaovendas.repositories.CategoriaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class CategoriaService {

    private final CategoriaRepository categoriaRepository;

    public List<Categoria> listAll() {
        return categoriaRepository.findAll();
    }

    public Optional<Categoria> findByCode(Long codigo) {
        return categoriaRepository.findById(codigo);
    }

    public Categoria save(Categoria categoria) {
        validateDuplicatedCategory(categoria);
        return categoriaRepository.save(categoria);
    }

    public Categoria update(Long code, Categoria categoria) {
        Categoria savedCategoria = validateCategoryExists(code);
        validateDuplicatedCategory(categoria);
        BeanUtils.copyProperties(categoria, savedCategoria, "codigo");
        return categoriaRepository.save(savedCategoria);
    }

    public void delete(Long code) {
        categoriaRepository.deleteById(code);
    }

    private Categoria validateCategoryExists(Long code) {
        Optional<Categoria> categoria = findByCode(code);
        return categoria.orElseThrow(() -> new EmptyResultDataAccessException(1));
    }

    private void validateDuplicatedCategory(Categoria categoria) {
        Categoria categoriaEncontrada = categoriaRepository.findByNome(categoria.getNome());
        if(Objects.nonNull(categoriaEncontrada) && !categoriaEncontrada.getCodigo().equals(categoria.getCodigo())) {
            throw new RegraNegocioException(String.format("A categoria %s já está cadastrada", categoria.getNome().toUpperCase()));
        }
    }


}

package br.com.bianeck.gvendas.gestaovendas.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Errors {

    private String msgUsuario;
    private String msgDesenvolvedor;

}
